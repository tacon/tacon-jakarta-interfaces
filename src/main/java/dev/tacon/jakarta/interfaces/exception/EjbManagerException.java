package dev.tacon.jakarta.interfaces.exception;

import dev.tacon.interfaces.exception.ManagerException;
import jakarta.ejb.ApplicationException;

@ApplicationException(rollback = true, inherited = true)
public class EjbManagerException extends ManagerException {

	private static final long serialVersionUID = -21838882422525335L;

	public EjbManagerException(final String errorCode, final String message, final Throwable cause) {
		super(errorCode, message, cause);
	}

	public EjbManagerException(final String errorCode, final String message) {
		super(errorCode, message);
	}

	public EjbManagerException(final String errorCode, final Throwable cause) {
		super(errorCode, cause);
	}

	public EjbManagerException(final String errorCode) {
		super(errorCode);
	}
}
