package dev.tacon.jakarta.interfaces.exception;

import java.util.List;

import dev.tacon.interfaces.exception.ManagerValidationException;
import jakarta.ejb.ApplicationException;

@ApplicationException(rollback = true, inherited = true)
public class EjbManagerValidationException extends ManagerValidationException {

	private static final long serialVersionUID = 7783602892758350421L;

	public EjbManagerValidationException(final List<ValidationError> validationErrors) {
		super(validationErrors);
	}
}
