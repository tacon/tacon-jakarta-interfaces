package dev.tacon.jakarta.interfaces.validation;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import dev.tacon.annotations.NonNull;
import dev.tacon.interfaces.exception.ManagerValidationException.ValidationError;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Path;
import jakarta.validation.Path.Node;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

public final class BeanValidator {

	private static final ValidatorFactory VALIDATION_FACTORY = Validation.buildDefaultValidatorFactory();

	public static @NonNull <T> List<ValidationError> validate(final @NonNull T data, final Class<?>... groups) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final Set<ConstraintViolation<T>> violations = validator.validate(data, groups);
		return violations.stream().map(BeanValidator::ofConstraintViolation).collect(Collectors.toCollection(ArrayList::new));
	}

	public static @NonNull <T, V> List<ValidationError> validateValue(final @NonNull Class<T> dataClass, final @NonNull String propertyName, final V value, final Class<?>... groups) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final Set<ConstraintViolation<T>> violations = validator.validateValue(dataClass, propertyName, value, groups);
		return violations.stream().map(c -> BeanValidator.ofConstraintViolation(c, dataClass, propertyName)).collect(Collectors.toCollection(ArrayList::new));
	}

	public static ValidationError ofConstraintViolation(final @NonNull ConstraintViolation<?> constraintViolation) {
		return ofConstraintViolation1(constraintViolation, null, null);
	}

	public static ValidationError ofConstraintViolation(final @NonNull ConstraintViolation<?> constraintViolation, final @NonNull Class<?> beanClass, final @NonNull String fieldName) {
		return ofConstraintViolation1(constraintViolation, beanClass, fieldName);
	}

	private static ValidationError ofConstraintViolation1(final @NonNull ConstraintViolation<?> constraintViolation, final Class<?> beanClass, final String fieldName) {
		boolean isField = false;
		final Path propertyPath2 = constraintViolation.getPropertyPath();
		String fullFieldName = null;
		if (fieldName != null) {
			final Field field = findField(beanClass, fieldName);
			isField = field != null;
			if (field != null) {
				fullFieldName = beanClass.getCanonicalName() + "." + field.getName();
			}
		} else {
			final Object leafBean = constraintViolation.getLeafBean();
			if (leafBean != null) {
				Node last = null;
				for (final Node node : propertyPath2) {
					last = node;
				}
				if (last != null) {
					try {
						final Class<? extends Object> leafBeanClass = leafBean.getClass();
						final Field beanField = findField(leafBeanClass, last.getName());
						fullFieldName = leafBeanClass.getCanonicalName() + "." + beanField.getName();
						isField = true;
					} catch (@SuppressWarnings("unused") final Exception e1) {}
				}
			}
		}
		final String messageTemplate = constraintViolation.getMessageTemplate();
		final Object invalidValue = constraintViolation.getInvalidValue();
		final Object rootBean = constraintViolation.getRootBean();
		return new ValidationError(constraintViolation.getConstraintDescriptor().getAnnotation(),
				rootBean instanceof Serializable ? (Serializable) rootBean : null,
				invalidValue instanceof Serializable ? (Serializable) invalidValue : null,
				isField,
				fullFieldName,
				Objects.toString(propertyPath2, null),
				constraintViolation.getMessage(),
				messageTemplate.startsWith("{") && messageTemplate.endsWith("}") ? messageTemplate.substring(1, messageTemplate.length() - 1) : null,
				constraintViolation.getConstraintDescriptor().getAttributes().entrySet().stream()
						.filter(e -> e.getValue() instanceof Serializable)
						.collect(Collectors.toMap(Entry::getKey, e -> (Serializable) e.getValue(), (a, b) -> a, HashMap::new)));
	}

	public static Field findField(final @NonNull Class<?> clazz, final @NonNull String name) {
		try {
			return clazz.getDeclaredField(name);
		} catch (final @SuppressWarnings("unused") NoSuchFieldException ex) {
			final Class<?> superclass = clazz.getSuperclass();
			return superclass == null ? null : findField(superclass, name);
		} catch (final @SuppressWarnings("unused") SecurityException ex) {
			return null;
		}
	}
}
